package com.stefanomantini.tools.restgateway.api.controller;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static com.stefanomantini.tools.restgateway.api.utils.TestResourceUtils.readResource;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.RestTemplate;

public class RestProxyResourceTest extends AbstractIntegrationTest {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  @Test
  public void test_wiremock_GET_OK() {
    // GIVEN
    wireMock.stubFor(
        get(urlEqualTo("/wiremock"))
            .willReturn(
                aResponse()
                    .withHeader("Content-Type", "application/json")
                    .withBody("{\"hello\":\"wiremock\"}")));
    // WHEN
    RestTemplate restTemplate = new RestTemplate();
    response = restTemplate.getForEntity(urlBuilder(false, "/wiremock"), String.class);
    // THEN
    assertThat("Verify Status Code", response.getStatusCode().equals(HttpStatus.OK));
    assertThat("Response Body is not null", !response.getBody().toString().isEmpty());
    debugResponse(response);
  }

  @Test
  public void test_actuator_health_GET_OK() {
    // WHEN
    RestTemplate restTemplate = new RestTemplate();
    response = restTemplate.getForEntity(urlBuilder(true, "/health"), String.class);
    // THEN
    assertThat("Verify Status Code", response.getStatusCode().equals(HttpStatus.OK));
    assertThat("Verify Body", response.getBody().toString().contains("UP"));
    debugResponse(response);
  }

  @Test
  public void GET_OK() {
    RestTemplate restTemplate = new RestTemplate();
    response = restTemplate.getForEntity(urlBuilder(true, "/api/hello"), String.class);
    assertThat("Verify Status Code", response.getStatusCode().equals(HttpStatus.OK));
    assertThat("Verify body", response.getBody().toString().contains("UP"));
  }

  @Test
  public void get_proxy_GET_OK() {
    // GIVEN
    wireMock.stubFor(
        get(urlEqualTo("/api/serviceName/serviceId"))
            .willReturn(
                aResponse()
                    .withHeader("Content-Type", "application/json")
                    .withHeader("Accept", "application/json")
                    .withBody(readResource("prms/GET_region.json"))
                    .withStatus(HttpStatus.OK.value())));
    // WHEN
    RestTemplate restTemplate = new RestTemplate();
    response =
        restTemplate.getForEntity(
            urlBuilder(true, "/api/serviceName/serviceId"), String.class);
    log.info(String.valueOf(response.getStatusCode()));
    log.info(String.valueOf(response.getBody()));
    log.info(String.valueOf(response.getHeaders()));
    // THEN
    assertThat("Verify Status Code", response.getStatusCode().equals(HttpStatus.OK));
    assertThat("Response is valid", !response.getBody().toString().isEmpty());
  }
}
