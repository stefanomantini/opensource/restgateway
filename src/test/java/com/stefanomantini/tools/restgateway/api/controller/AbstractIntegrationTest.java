package com.stefanomantini.tools.restgateway.api.controller;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class AbstractIntegrationTest {

  @Rule public WireMockRule wireMock = new WireMockRule(wireMockConfig().dynamicPort());

  protected RestTemplate restTemplate;
  protected ResponseEntity response;

  protected String base = "http://localhost:";
  protected int wiremockPort;
  protected @LocalServerPort int localServerPort;

  @Before
  public void setup() {
    restTemplate = new RestTemplate();
    response = null;
    wiremockPort = wireMock.port();
  }

  /**
   * True for API, false for Wiremock
   *
   * @param isApi to hit endpoint, false to hit wiremock
   * @param uri path to request
   * @return built uri
   */
  protected String urlBuilder(Boolean isApi, String uri) {
    String url = isApi ? base + localServerPort + uri : base + wiremockPort + uri;
    System.out.println("URL: " + url);
    return url;
  }

  /**
   * Debug responses in console output
   *
   * @param debugResponse
   */
  protected void debugResponse(ResponseEntity debugResponse) {
    System.out.println("Status: " + debugResponse.getStatusCode().toString());
    System.out.println("Body: " + debugResponse.getBody().toString());
  }
}
