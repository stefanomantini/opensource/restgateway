package com.stefanomantini.tools.restgateway;

import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import com.stefanomantini.tools.restgateway.Application;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class ApplicationTest {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  @Autowired Environment env;

  @Test
  public void contextLoads() {}

  @Test
  public void checkBaseActiveProfiles() {
    List<String> baseProfiles = Arrays.asList("base", "logging", "server");
    List<String> activeProfiles = Arrays.asList(env.getActiveProfiles());
    for (String baseProfile : baseProfiles) {
      assertTrue(activeProfiles.contains(baseProfile));
    }
  }
}
