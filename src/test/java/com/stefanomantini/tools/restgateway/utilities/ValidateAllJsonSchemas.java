package com.stefanomantini.tools.restgateway.utilities;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.test.context.junit4.SpringRunner;
import com.stefanomantini.tools.restgateway.service.config.ServiceConstants;
import com.stefanomantini.tools.restgateway.service.contract.ConfigurationService;
import com.stefanomantini.tools.restgateway.service.contract.SchemaValidationService;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ValidateAllJsonSchemas {

  private static final String JSON_SCHEMA_FILE_PATTERN = ".schema.json";

  @Autowired ConfigurationService configurationService;
  @Autowired SchemaValidationService schemaValidationService;
  @Autowired ResourcePatternResolver resourceResolver;

  @Test
  public void getAllJsonSchemas() {
    List<Resource> resourceFileNames =
        configurationService.getClasspathResources(ServiceConstants.JSON_SCHEMA_CONFIG_LOCATION);

    // assumption is made that for every schema file there will be a corresponding json mock file
    List<Resource> schemaFiles =
        resourceFileNames.stream()
            .filter(file -> file.getFilename().matches("(.*)(?:.schema.json)"))
            .collect(Collectors.toList());

    schemaFiles.forEach(file -> System.out.println(file.getFilename()));

    schemaFiles.forEach(
        file -> {
          Resource schemaFile = file;
          Resource mockFile =
              configurationService
                  .getClasspathResources(file.getFilename().split(".")[0] + ".json")
                  .get(0);
          assertTrue(
              "schema Validation for: " + file,
              schemaValidationService.isSchemaValid(schemaFile, mockFile));
        });
  }
}
