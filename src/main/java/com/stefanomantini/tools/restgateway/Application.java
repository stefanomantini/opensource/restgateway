package com.stefanomantini.tools.restgateway;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;
import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.core.env.Environment;
import org.springframework.core.env.SimpleCommandLinePropertySource;
import com.stefanomantini.tools.restgateway.service.config.ServiceConstants;
import com.stefanomantini.tools.restgateway.service.contract.ProfileService;

@SpringBootApplication
public class Application extends SpringBootServletInitializer {

  private static final Logger log = LoggerFactory.getLogger(Application.class);

  @Autowired private Environment env;

  @Autowired ProfileService profileService;

  /** Configure entrypoint for servlet */
  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
    return application.sources(Application.class);
  }

  /** Spring profiles can be configured with a program arguments --spring.profiles.active=profile */
  @PostConstruct
  public void initApplication() throws IOException {
    List<String> activeProfiles = Arrays.asList(env.getActiveProfiles());
    if (!activeProfiles.containsAll(ServiceConstants.BASE_PROFILES)) {
      log.error("Profile configuration does not include base profiles");
    } else {
      log.info("Running with profile(s) : {}", activeProfiles);
    }
  }

  /** Main method, used to run the application. */
  public static void main(String[] args) throws UnknownHostException {
    SpringApplication app = new SpringApplication(Application.class);
    SimpleCommandLinePropertySource source = new SimpleCommandLinePropertySource(args);
    addDefaultProfile(app, source);
    Environment env = app.run(args).getEnvironment();
    log.info(
        "Access URLs:\n----------------------------------------------------------\n\t"
            + "Local: \t\thttp://127.0.0.1:{}\n\t"
            + "External: \thttp://{}:{}\n----------------------------------------------------------",
        env.getProperty("local.server.port"),
        InetAddress.getLocalHost().getHostAddress(),
        env.getProperty("local.server.port"));
  }

  /** If no profile has been configured, set default profiles */
  private static void addDefaultProfile(
      SpringApplication app, SimpleCommandLinePropertySource source) {
    // add base profiles
    if (!source.containsProperty(ServiceConstants.SPRING_PROFILES_ACTIVE_KEY_PROP)
        && !System.getenv().containsKey(ServiceConstants.SPRING_PROFILES_ACTIVE_KEY_SH)) {
      for (String baseProfile : ServiceConstants.BASE_PROFILES) {
        app.setAdditionalProfiles(baseProfile);
      }
    }
  }
}
