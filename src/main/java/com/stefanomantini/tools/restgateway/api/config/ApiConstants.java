package com.stefanomantini.tools.restgateway.api.config;

import java.util.Arrays;
import java.util.List;

public final class ApiConstants {

  public static final List<String> HEADER_REWRITE_EXCLUSIONS =
      Arrays.asList(
          "Content-Type",
          "Accept",
          "Access-Control-Allow-Credentials",
          "Access-Control-Allow-Origin",
          "Access-Control-Allow-Methods");

  public static final String INTERNAL_SERVER_ERROR = "{Internal Server Error";
    public static final String HEADER_CORRELATION_ID = "correlationId";
}
