// package uk.gov.hmrc.api.config;
//
// import com.fasterxml.jackson.annotation.JsonInclude;
// import com.fasterxml.jackson.databind.ObjectMapper;
// import com.fasterxml.jackson.databind.SerializationFeature;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.beans.factory.annotation.Value;
// import org.springframework.boot.api.client.RestTemplateBuilder;
// import org.springframework.context.annotation.Bean;
// import org.springframework.context.annotation.Configuration;
// import org.springframework.context.annotation.Import;
// import org.springframework.http.HttpHeaders;
// import org.springframework.http.HttpRequest;
// import org.springframework.http.MediaType;
// import org.springframework.http.client.*;
// import org.springframework.http.client.support.BasicAuthorizationInterceptor;
// import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
// import org.springframework.stereotype.Component;
//
// import java.io.IOException;
// import java.nio.charset.Charset;
// import java.util.ArrayList;
// import java.util.Collections;
// import java.util.List;
//
// import static java.lang.Integer.parseInt;
//
// @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
// @Configuration
// @Import(HttpClientConfig.class)
// public class RestTemplateConfig {
//
//  private static final String DEFAULT_CHARSET_NAME = Charset.defaultCharset().name();
//
//  @Bean
//  public RestTemplateBuilder restTemplateBuilder(final ClientHttpRequestFactory
// clientHttpRequestFactory) {
//
//    final MappingJackson2HttpMessageConverter messageConverter =
//        new MappingJackson2HttpMessageConverter();
//
//    final ObjectMapper objectMapper = messageConverter.getObjectMapper();
//    objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
//    objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
//
//    final List<ClientHttpRequestInterceptor> clientHttpRequestInterceptors = new ArrayList<>();
//
////    clientHttpRequestInterceptors.add(headersInterceptor);
////    clientHttpRequestInterceptors.add(apiLoggingInterceptor);
////    clientHttpRequestInterceptors.add(new BasicAuthorizationInterceptor(username, password));
//
//    return new RestTemplateBuilder()
//        .messageConverters(Collections.singletonList(messageConverter))
//        .requestFactory(new BufferingClientHttpRequestFactory(clientHttpRequestFactory))
//        .additionalInterceptors(clientHttpRequestInterceptors)
//        .errorHandler(new RestTemplateResponseErrorHandler());
//  }
//
//  //  @Component
//  //  private static class HeadersInterceptor implements ClientHttpRequestInterceptor {
//  //
//  //    //    @Value("${remote.service.des.header.environment}")
//  //    //    private String environment;
//  //
//  //    //        @Autowired
//  //    //        private ProcessingContext processingContext;
//  //
//  //    @Override
//  //    public ClientHttpResponse intercept(
//  //        final HttpRequest request, final byte[] body, final ClientHttpRequestExecution
//  // execution)
//  //        throws IOException {
//  //
//  //      final HttpHeaders headers = request.getHeaders();
//  //      //            headers.set(RemoteConstants.HEADER_CORRELATION_ID,
//  //      // processingContext.getCorrelationId());
//  //      //            headers.set(RemoteConstants.HEADER_USER_ID,
// processingContext.getUserId());
//  //      headers.setContentType(MediaType.APPLICATION_JSON);
//  //
//  //      return execution.execute(request, body);
//  //    }
//  //  }
//
//  //    @Component
//  //    private static class ApiLoggingInterceptor implements ClientHttpRequestInterceptor {
//  //
//  //        @Autowired private ProcessingContext processingContext;
//  //
//  //        @Autowired private ApiLoggingService apiLoggingService;
//  //
//  //        @Override
//  //        public ClientHttpResponse intercept(
//  //                final HttpRequest request, final byte[] body, final ClientHttpRequestExecution
//  // execution)
//  //                throws IOException {
//  //            final String correlationId = processingContext.getCorrelationId();
//  //
//  //            final String method = request.getMethod().toString();
//  //            final String requestUrl = request.getURI().toString();
//  //
//  //            final Long requestLoggingId =
//  //                    apiLoggingService.logRequest(
//  //                            ApiLoggingService.DirectionType.OUTBOUND,
//  //                            method,
//  //                            requestUrl,
//  //                            correlationId,
//  //                            StringUtils.defaultIfEmpty(new String(body, DEFAULT_CHARSET_NAME),
//  // null));
//  //
//  //            final ClientHttpResponse response = execution.execute(request, body);
//  //
//  //            apiLoggingService.logResponse(
//  //                    ApiLoggingService.DirectionType.OUTBOUND,
//  //                    requestLoggingId,
//  //                    method,
//  //                    requestUrl,
//  //                    correlationId,
//  //                    StringUtils.defaultIfEmpty(
//  //                            IOUtils.toString(response.getBody(), DEFAULT_CHARSET_NAME), null),
//  //                    response.getRawStatusCode());
//  //
//  //            return response;
//  //        }
//  //    }
//
// }
