// package uk.gov.hmrc.api.config;
//
// import com.fasterxml.jackson.databind.ObjectMapper;
// import org.springframework.http.HttpStatus;
// import org.springframework.http.client.ClientHttpResponse;
// import org.springframework.http.converter.HttpMessageNotReadableException;
// import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
// import org.springframework.stereotype.Component;
// import org.springframework.api.client.ResponseErrorHandler;
// import RemoteGatewayException;
// import RemoteServiceException;
// import ErrorDTO;
//
// import java.io.IOException;
//
// @Component
// public class RestTemplateResponseErrorHandler implements ResponseErrorHandler {
//
//  private MappingJackson2HttpMessageConverter messageConverter =
//      new MappingJackson2HttpMessageConverter(new ObjectMapper());
//
//  @Override
//  public void handleError(ClientHttpResponse httpResponse) throws IOException {
//    try {
//      final ErrorDTO errorDto = (ErrorDTO) messageConverter.read(ErrorDTO.class, httpResponse);
//      final HttpStatus httpStatus = httpResponse.getStatusCode();
//      throw new RemoteServiceException(httpStatus, errorDto);
//    } catch (HttpMessageNotReadableException e) {
//      // This shouldn't happen in a real environment but can when using wiremock
//      throw new RemoteGatewayException(
//          "Remote service returned an unreadable HTTP Error response", e);
//    }
//  }
//
//  @Override
//  public boolean hasError(ClientHttpResponse httpResponse) throws IOException {
//    return (httpResponse.getStatusCode().series() == HttpStatus.Series.CLIENT_ERROR
//        || httpResponse.getStatusCode().series() == HttpStatus.Series.SERVER_ERROR);
//  }
// }
