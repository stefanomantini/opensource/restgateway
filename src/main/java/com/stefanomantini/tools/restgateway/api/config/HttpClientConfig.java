// package uk.gov.hmrc.api.config;
//
// import org.apache.http.impl.client.CloseableHttpClient;
// import org.apache.http.impl.client.HttpClientBuilder;
// import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
// import org.springframework.beans.factory.annotation.Value;
// import org.springframework.context.annotation.Bean;
// import org.springframework.context.annotation.Configuration;
// import org.springframework.http.client.ClientHttpRequestFactory;
// import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
// import org.apache.http.client.config.RequestConfig;
//
// @Configuration
// public class HttpClientConfig {
//
//    @Bean
//    public ClientHttpRequestFactory httpClient(
//            @Value("${base.proxy.request.timeout}") int timeout,
//            @Value("${base.proxy.request.poolTotal}") int maxTotal,
//            @Value("${base.proxy.request.poolPerRoute}") int defaultMaxPerRoute) {
//
//        final RequestConfig config =
//                RequestConfig.custom()
//                        .setConnectTimeout(timeout)
//                        .setConnectionRequestTimeout(timeout)
//                        .setSocketTimeout(timeout)
//                        .build();
//
//        final PoolingHttpClientConnectionManager connManager = new
// PoolingHttpClientConnectionManager();
//        connManager.setMaxTotal(maxTotal);
//        connManager.setDefaultMaxPerRoute(defaultMaxPerRoute);
//
//        final CloseableHttpClient httpClient =
//                HttpClientBuilder.create()
//                        .setDefaultRequestConfig(config)
//                        .setConnectionManager(connManager)
//                        .disableRedirectHandling()
//                        .build();
//
//        return new HttpComponentsClientHttpRequestFactory(httpClient);
//    }
// }
//
