package com.stefanomantini.tools.restgateway.api.exception;

import java.util.Collections;
import java.util.List;
import org.springframework.http.HttpStatus;
import com.stefanomantini.tools.restgateway.api.model.ErrorDTO;

public class RemoteServiceException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  private final ErrorDTO errorDto;
  private final HttpStatus httpStatus;

  public RemoteServiceException(HttpStatus httpStatus, ErrorDTO errorDto) {
    this.errorDto = errorDto;
    this.httpStatus = httpStatus;
  }

  public String getMessage() {
    return this.httpStatus.getReasonPhrase();
  }

  public HttpStatus getHttpStatus() {
    return this.httpStatus;
  }

  public List<String> getErrors() {
    return Collections.singletonList(errorDto.getCode() + ": " + errorDto.getReason());
  }

  public boolean reasonCodeIs(String code) {
    return this.errorDto.getCode().equalsIgnoreCase(code);
  }
}
