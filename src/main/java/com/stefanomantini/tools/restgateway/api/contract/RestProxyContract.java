package com.stefanomantini.tools.restgateway.api.contract;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import javax.servlet.http.HttpServletRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

@Api("RestProxy")
public interface RestProxyContract {

  /**
   * This contract defines the entrypoint of all HIB interaction
   *
   * @param request
   * @param headers
   * @param body
   * @param context
   * @return
   */
  @ApiOperation(
      value = "Rest Proxy Interceptor Endpoint",
      notes = "Applies rules to intercepted APIs and redirects to appropriate hosts and endpoints")
  @ApiResponses({
    @ApiResponse(code = 200, message = "Successful response from client"),
    @ApiResponse(code = 400, message = "Request has failed validation from client"),
    @ApiResponse(code = 404, message = "Resource not found from client"),
    @ApiResponse(code = 500, message = "Error while processing the request")
  })
  ResponseEntity<String> restProxy(
      HttpServletRequest request, HttpHeaders headers, String body, String context);
}
