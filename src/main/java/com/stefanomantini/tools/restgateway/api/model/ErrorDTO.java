package com.stefanomantini.tools.restgateway.api.model;

public class ErrorDTO {
  private String code = "UNKNOWN";
  private String reason = "unknown";

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getReason() {
    return reason;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }
}
