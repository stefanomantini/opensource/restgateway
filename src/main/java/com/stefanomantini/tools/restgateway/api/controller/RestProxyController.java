package com.stefanomantini.tools.restgateway.api.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.stefanomantini.tools.restgateway.api.contract.RestProxyContract;
import com.stefanomantini.tools.restgateway.service.contract.LogService;
import com.stefanomantini.tools.restgateway.service.contract.RestProxyService;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

@RestController
@RequestMapping(value = "/api/")
public class RestProxyController implements RestProxyContract {

  private final Logger log = Logger.getLogger(this.getClass());

  @Autowired RestProxyService restProxyService;

  @Autowired LogService logService;

  @RequestMapping(
      value = "{context}",
      method = {
        RequestMethod.GET, RequestMethod.PUT, RequestMethod.POST, RequestMethod.DELETE,
        RequestMethod.PATCH, RequestMethod.HEAD, RequestMethod.OPTIONS, RequestMethod.TRACE
      },
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<String> restProxy(
      HttpServletRequest request,
      @RequestHeader() HttpHeaders headers,
      @RequestBody(required = false) String body,
      @PathVariable() final String context) {

    final String requestResponseId = UUID.randomUUID().toString();

    logService.logRequest(request, headers, body, context, requestResponseId);

    ResponseEntity<String> response =
        restProxyService.executeRequest(
            HttpMethod.resolve(request.getMethod()),
            headers,
            body,
            request.getLocalPort(),
            context);

    logService.logResponse(response, requestResponseId);

    return response;
  }
}
