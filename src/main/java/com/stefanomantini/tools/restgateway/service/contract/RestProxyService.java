package com.stefanomantini.tools.restgateway.service.contract;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

public interface RestProxyService {

  HttpHeaders addBaseHeaders(HttpHeaders passedHeaders);

  ResponseEntity<String> executeRequest(
      HttpMethod method, HttpHeaders headers, String body, int tempPort, String context);
}
