package com.stefanomantini.tools.restgateway.service.contract;

import org.springframework.core.io.Resource;

import java.util.List;

public interface ConfigurationService {

  String getFunctionalParam(String selector);

  String resolveProxyPath(String serviceId, String serviceContext);

  List<String> getResourceFileNames(String location);

  List<Resource> getClasspathResources(String location);
}
