package com.stefanomantini.tools.restgateway.service.impl;

import com.stefanomantini.tools.restgateway.service.contract.ConfigurationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class ConfigurationServiceImpl implements ConfigurationService {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  @Autowired ResourcePatternResolver resourceResolver;

  @Override
  public String getFunctionalParam(String selector) {
    return null;
  }

  @Override
  public String resolveProxyPath(String serviceId, String serviceContext) {
    return null;
  }

  @Override
  public List<String> getResourceFileNames(String location) {
    List<String> files = new ArrayList<>();
    for (Resource resource : getClasspathResources(location)) {
      files.add(resource.getFilename());
    }
    return files;
  }

  @Override
  public List<Resource> getClasspathResources(String location) {
    try {
      List<Resource> profileConfigRes;
      profileConfigRes =
              Arrays.asList(
                      resourceResolver.getResources(location));
      return profileConfigRes;
    } catch (IOException e) {
      log.error("Don't delete all of the yml config files!!");
    }
    // we shouldn't ever get here
    return null;
  }
}
