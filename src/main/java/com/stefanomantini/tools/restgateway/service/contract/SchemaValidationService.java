package com.stefanomantini.tools.restgateway.service.contract;

import org.springframework.core.io.Resource;

public interface SchemaValidationService {

  Boolean isSchemaValid(Resource schemaFile, Resource jsonFile);
}
