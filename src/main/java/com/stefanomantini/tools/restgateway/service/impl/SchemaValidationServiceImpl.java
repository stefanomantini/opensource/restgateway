package com.stefanomantini.tools.restgateway.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.stefanomantini.tools.restgateway.service.contract.SchemaValidationService;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class SchemaValidationServiceImpl implements SchemaValidationService {

  @Override
  public Boolean isSchemaValid(Resource schemaFile, Resource jsonFile) {
    final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
    JsonSchema jsonSchema;
    try {
      jsonSchema = factory.getJsonSchema(schemaFile.getFile().getAbsolutePath());
      ObjectMapper mapper = new ObjectMapper();
      JsonNode schemaJson = mapper.readTree(jsonFile.getFile().getAbsolutePath());
      return jsonSchema.validate(schemaJson).isSuccess();
    } catch (ProcessingException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    // should never get here
    return null;
  }

  private String getFileContentsAsString(String filePath){
    return null
  }

}
