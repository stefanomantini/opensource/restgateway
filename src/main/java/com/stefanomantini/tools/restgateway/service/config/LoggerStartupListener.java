package com.stefanomantini.tools.restgateway.service.config;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.LoggerContextListener;
import ch.qos.logback.core.Context;
import ch.qos.logback.core.spi.ContextAwareBase;
import ch.qos.logback.core.spi.LifeCycle;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.util.Random;

public class LoggerStartupListener extends ContextAwareBase
    implements LoggerContextListener, LifeCycle {

  private final org.slf4j.Logger log = LoggerFactory.getLogger(this.getClass());

  private boolean started = false;

  @Override
  public void start() {
    if (started) return;
    String jbossHost = System.getProperty(ServiceConstants.JBOSS_SERVER_NAME_PROPERTY);
    if (StringUtils.isEmpty(jbossHost)) {
      log.info(ServiceConstants.APP_NOT_RUNNING_ON_JBOSS);
      jbossHost =
          Long.toString(Math.abs(new Random().nextLong()), ServiceConstants.GUID_LENGTH)
              .substring(0, ServiceConstants.MAX_HOSTNAME_LENGTH);
    }
    Context context = getContext();
    context.putProperty(ServiceConstants.INSTANCE_NAME_PROPERTY, jbossHost);
    log.info(ServiceConstants.APP_RUNNING_ON_SERVER, jbossHost);
    started = true;
  }

  @Override
  public void stop() {}

  @Override
  public boolean isStarted() {
    return started;
  }

  @Override
  public boolean isResetResistant() {
    return true;
  }

  @Override
  public void onStart(LoggerContext context) {}

  @Override
  public void onReset(LoggerContext context) {}

  @Override
  public void onStop(LoggerContext context) {}

  @Override
  public void onLevelChange(Logger logger, Level level) {
    log.info("Log level changed to: " + level.levelStr);
  }
}
