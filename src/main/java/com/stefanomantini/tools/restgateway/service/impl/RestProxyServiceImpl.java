package com.stefanomantini.tools.restgateway.service.impl;

import com.stefanomantini.tools.restgateway.api.config.ApiConstants;
import com.stefanomantini.tools.restgateway.api.exception.RemoteGatewayException;
import com.stefanomantini.tools.restgateway.service.contract.ConfigurationService;
import com.stefanomantini.tools.restgateway.service.contract.LogService;
import com.stefanomantini.tools.restgateway.service.contract.RestProxyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

@Service
public class RestProxyServiceImpl implements RestProxyService {

  private final Logger log = LoggerFactory.getLogger(this.getClass());

  RestTemplate restTemplate;

  @Autowired private ConfigurationService configurationService;

  @Autowired private LogService logService;

  /**
   * Adds base headers to HTTP requests
   *
   * @param clientHeader
   * @return
   */
  @Override
  public HttpHeaders addBaseHeaders(HttpHeaders clientHeader) {
    HttpHeaders baseHeaders = new HttpHeaders();
    baseHeaders.setContentType(MediaType.APPLICATION_JSON);
    Map<String, String> clientHeaderMap = clientHeader.toSingleValueMap();
    for (Map.Entry<String, String> header : clientHeaderMap.entrySet()) {
      if (!ApiConstants.HEADER_REWRITE_EXCLUSIONS.contains(header.getKey())) {
        baseHeaders.set(header.getKey(), header.getValue());
      }
    }
    return baseHeaders;
  }

  /**
   * Performs the async controller template request
   *
   * @param method
   * @param headers
   * @param body
   * @param context
   * @return
   */
  @Override
  public ResponseEntity<String> executeRequest(
      HttpMethod method, HttpHeaders headers, String body, int tempPort, String context) {
    ResponseEntity<String> result = null;
    //        String uri = configurationService.resolveProxyPath(serviceName, serviceContext);
    String uri = "http://localhost:" + tempPort + context;
    try {
      result =
          restTemplate.exchange(
              "http://localhost:" + tempPort + context,
              method,
              new HttpEntity<>(headers),
              String.class);
      log.info(uri);
    } catch (NullPointerException npe) {
      throw new RemoteGatewayException(
          "Cannot connect to remote service "
              + uri
              + " HTTP Status: "
              + result.getStatusCode().toString(),
          npe);
    }
    return result;
  }
}
