package com.stefanomantini.tools.restgateway.service.config;

import java.util.Arrays;
import java.util.List;

public class ServiceConstants {
  public static final List<String> BASE_PROFILES = Arrays.asList("base", "logging", "server");
  public static final String SPRING_PROFILE_CONFIG_LOCATION = "classpath:configuration/**/*.yml";
  public static final String JSON_SCHEMA_CONFIG_LOCATION = "classpath:configuration/**/*.json";
  public static final String SPRING_PROFILE_FILE_FORMAT = ".yml";

  public static final String SPRING_PROFILES_ACTIVE_KEY_PROP = "spring.profiles.active";
  public static final String SPRING_PROFILES_ACTIVE_KEY_SH = "SPRING_PROFILES_ACTIVE";

  public static final String HIB_SERVICE_ID_KEY_PROP = "hib.service.id";
  public static final String HIB_SERVICE_ID_KEY_SH = "HIB_SERVICE_ID";

  public static final String INSTANCE_NAME_PROPERTY = "JBOSS_SERVER_NAME";
  public static final String JBOSS_SERVER_NAME_PROPERTY = "jboss.server.name";
  public static final String APP_NOT_RUNNING_ON_JBOSS = "App not running on JBoss";
  public static final int GUID_LENGTH = 10;
  public static final int MAX_HOSTNAME_LENGTH = 10;
  public static final String APP_RUNNING_ON_SERVER = "App running on Server";
}
